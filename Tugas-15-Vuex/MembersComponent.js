export const MembersComponent = {
	data:function(){
		return{

		}
	},
	computed:{
		members(){
			return this.$store.state.members
		}
	},

	methods:{
		handleDelete(id){
			this.$store.dispatch('handleDelete', id)
		},

		handleEdit(member){
			this.$store.commit('handleEdit', member)
		},

		handlePhoto(member){
			this.$store.commit('handlePhoto', member)
		}
	},

	mounted(){
		this.$store.dispatch('getMembers')
	},

	template : `
	<div>
		<table>
			<thead>
				<tr>
					<th>Photo</th>
					<th>Data</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<tr v-for="member in members">
					<td><img width="100" :src="member.photo_profile ? 'http://demo-api-vue.sanbercloud.com'+member.photo_profile : 'https://mm.widyatama.ac.id/wp-content/uploads/2020/08/dummy-profile-pic-male1.jpg'"></td>
					<td ><b>Nama :</b>{{member.name}} <br>
						 <b>Alamat :</b>{{member.address}} <br>
						 <b>No HP :</b>{{member.no_hp}} <br>
					</td>
					<td>
						<button @click="handleEdit(member)">Edit</button>
						<button @click="handleDelete(member.id)">Hapus</button>
						<button @click="handlePhoto(member)">Upload Foto</button>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	`,
}