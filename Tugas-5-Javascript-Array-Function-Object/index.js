console.log("soal 1");
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

for (var i = 0; i <= daftarHewan.length -1; i++) {
	var urut = daftarHewan.sort();
	console.log(urut[i]);
}

console.log(" ");
console.log("soal 2");

function introduce(data){
	return "Nama saya "+data.name+" umur saya "+data.age+" tahun, alamat saya di "+data.address+" ,dan saya punya hobby yaitu "+data.hobby+" !"
}

var data = { name:"John", age: 30, address: "Jalan Pelesiran", hobby:"Gaming" };

var perkenalan = introduce(data);
console.log(perkenalan);

console.log(" ");
console.log("soal 3");

function hitung_huruf_vokal(data){
	var arr = data.toLowerCase().split("");
	var hurufVokal = 0;
	for (var i = 0 - 1; i <= arr.length; i++) {
		if (arr[i]==="a" || arr[i]=== "i" || arr[i]=== "u" || arr[i]=== "e" || arr[i]=== "o") {
			hurufVokal++
		}
	}
	return hurufVokal;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");
var hitung_2 = hitung_huruf_vokal("Iqbal");
console.log(hitung_1, hitung_2);

console.log(" ");
console.log("soal 4");

function hitung(data){
	var angka = (data-1)*2;
	return angka;
}

console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));