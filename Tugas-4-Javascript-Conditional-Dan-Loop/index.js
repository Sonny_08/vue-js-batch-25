console.log(" ");
console.log("soal 1");

var nilai = 75;

if (nilai<=100 && nilai>=0) {
	if (nilai>=85) {
		console.log("A");
	} else if (nilai>=75 && nilai<85) {
		console.log("B");
	} else if (nilai>=65 && nilai<75) {
		console.log("C");
	} else if (nilai>=55 && nilai<65) {
		console.log("D");
	} else {
		console.log("E");
	}
} else {
	console.log("nilai harus lebih besar 0 dan kurang dari 100");
}

console.log(" ");
console.log("soal 2");

var tanggal = 24;
var bulan = 12;
var tahun = 1989;

switch(bulan){
	case 1: {console.log(tanggal+" Januari "+tahun); break;}
	case 2: {console.log(tanggal+" Februari "+tahun); break;}
	case 3: {console.log(tanggal+" Maret "+tahun); break;}
	case 4: {console.log(tanggal+" April "+tahun); break;}
	case 5: {console.log(tanggal+" Mei "+tahun); break;}
	case 6: {console.log(tanggal+" Juni "+tahun); break;}
	case 7: {console.log(tanggal+" Juli "+tahun); break;}
	case 8: {console.log(tanggal+" Agustus "+tahun); break;}
	case 9: {console.log(tanggal+" September "+tahun); break;}
	case 10: {console.log(tanggal+" Oktober "+tahun); break;}
	case 11: {console.log(tanggal+" Nopember "+tahun); break;}
	case 12: {console.log(tanggal+" Desember "+tahun); break;}
	default: {console.log("bulan tidak ada");}
}

console.log(" ");
console.log("soal 3");

var pagar = "";
var n = 7;

for (var i = 0; i < n; i++) {
	for (var j = 0; j <= i; j++) {
		pagar += '#';
	}
	pagar += '\n';
}

console.log(pagar);

console.log(" ");
console.log("soal 4");

var m = 10;

for (var i = 1; i <= m; i++) {
	if (i%3 === 1) {
		console.log(i+" - I love programming");
	} else if (i%3 === 2) {
		console.log(i+" - I love Javascript");
	} else {
		console.log(i+" - I love VueJS \n ===");
	}
}





