var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 

let time = 10000
let index = 0

const execute = (time, index)=> {
    readBooksPromise(time, books[index])
    .then((time, indexBaru)=>{
        if( time !== 0 && index < books.length - 1 ){
            execute(time, index+1)
        }
    }).catch((error)=>{
        console.log(error)
    })
}

execute(time, index)