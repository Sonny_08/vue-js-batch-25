// soal 1
console.log("soal 1");

const luasPersegiPanjang = (panjang, lebar)=>{
	const Luas = panjang*lebar; 
	return Luas;
};

const kelilingPersegiPanjang = (panjang, lebar)=>{
	const Keliling = 2*(panjang+lebar); 
	return Keliling;
};

console.log(luasPersegiPanjang(4,6));
console.log(kelilingPersegiPanjang(4,6));

console.log(" ");

// soal 2
console.log("soal 2");

const newFunction = (firstName, lastName) =>{
  return { firstName, lastName, fullName:()=> console.log(firstName + " " + lastName)};
}

newFunction("William", "Imoh").fullName();

console.log(" ");

// soal 3
console.log("soal 3");

const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};

const {firstName, lastName, address, hobby} = newObject;

console.log(firstName, lastName, address, hobby);

console.log(" ");

// soal 4
console.log("soal 4");

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];

const combined = [...west,...east];

console.log(combined);

console.log(" ");

// soal 5
console.log("soal 5");

const planet = "earth"; 
const view = "glass"; 
var before = `Lorem ${view}dolor sit amet, consectetur adipiscing elit, ${planet}`;

console.log(before) 

